package com.interview.darkstone;

import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import au.com.bytecode.opencsv.CSVReader;

public class HotMess {
	
	public static class Matrix{
        double [][]data = null;
        int cols = 0;
        int rows = 0;

        Matrix(int rows, int cols){
        	data = new double[cols][rows];
        	this.cols = cols;
        	this.rows = rows;
        }
        
        double[][] getTrainingData(int holdBackDays){
        	double [][]copy = new double[cols][rows - holdBackDays];

        	for (int col=0; col<cols; col++){
        		for (int row=0; row<(rows-holdBackDays); row++){
        			copy[col][row] = data[col][row];
        		}
        	}
        	return copy;
        }
        
        double[][] getTestData(int holdBackDays){
        	double [][]copy = new double[cols][holdBackDays];

        	int rowoffset = (rows-holdBackDays);
        	for (int col=0; col<cols; col++){
        		for (int row=rowoffset; row < rows; row++){
        			copy[col][row-rowoffset] = data[col][row];
        		}
        	}
        	return copy;
        }
        
	}
	
	/**
	 * Prediction model #1 - random numbers...
	 *
	 */
	public static class PredictorRandom{
		double [][]data = null;
        int cols = 0;
        int rows = 0;

		public PredictorRandom(double [][]data){
			this.data = data;
			cols = data.length;
			rows = data[0].length;
		}
		
		double[][] next30Days(){
        	double [][]forecast = new double[data.length][30];
        	int fcols = forecast.length;
        	int frows = 30;
        	Random rand = new Random();
			
        	for (int col=0; col<fcols; col++){
            	for (int row=0; row<frows; row++){
            		forecast[col][row] = rand.nextDouble();
            	}
        	}
        	
        	return forecast;
		}
	}
	
	/**
	 * Prediction model #2 - come up with a prediction based on 
	 * averaging some number of days.
	 *
	 */
	public static class PredictorRollingAverage{
		double [][]data = null;
        int cols = 0;
        int rows = 0;
        int daysHistoryUsed = 0;

		public PredictorRollingAverage(double [][]data, int daysHistoryUsed){
			this.data = data;
			cols = data.length;
			rows = data[0].length;
			this.daysHistoryUsed = daysHistoryUsed;
		}
		
		double[][] nextXDays(int numDays){
        	double [][]forecast = new double[data.length][numDays];
        	double avg[] = new double[data.length];
        	int fcols = forecast.length;
        	int frows = daysHistoryUsed;

        	// Col avg - last daysHistoryUsed days.
        	int rowoffset = rows - daysHistoryUsed - 30;
        	for (int col=0; col<cols; col++){
        		
        		double sum = 0f;
        		for (int row=rowoffset; row < rows; row++){
        			 sum += data[col][row];
        		}
        		avg[col] = ((double)sum) / daysHistoryUsed;
        	}
        	
        	
        	for (int col=0; col<fcols; col++){
            	for (int row=0; row<numDays; row++){
            		forecast[col][row] = avg[col];
            	}
        	}
        	
        	return forecast;
		}
	}	
	

	/**
	 * Prediction model #3 - PolynomialFunctionLagrangeForm...
	 *
	 */
	public static class PredictorPFLF{
		double [][]data = null;
        int cols = 0;
        int rows = 0;
        int daysHistoryUsed = 0;

		public PredictorPFLF(double [][]data, int daysHistoryUsed){
			this.data = data;
			cols = data.length;
			rows = data[0].length;
			this.daysHistoryUsed = daysHistoryUsed;
		}
		
		double[][] nextXDays(int numDays){
        	double [][]forecast = new double[data.length][numDays];
        	double avg[] = new double[data.length];
        	int fcols = forecast.length;
        	int frows = daysHistoryUsed;
        	Random rand = new Random();
        	boolean cumulative = true;

        	double x[] = new double[daysHistoryUsed];
        	double y[] = new double[daysHistoryUsed];
        	for (int d=0; d<daysHistoryUsed; d++){
        		x[d] = d;
        	}
        	
        	// last daysHistoryUsed days.
        	int rowoffset = rows - daysHistoryUsed;
        	for (int col=0; col<cols; col++){        		
            	print("PRLF %d\n", col);
        		for (int row=rowoffset; row < rows; row++){
        			 y[row - rowoffset] = data[col][row];
        			 if (cumulative && row != rowoffset) y[row - rowoffset] += y[row - rowoffset - 1];
        			 print("%f\n", y[row - rowoffset]);        			 
        		}
                PolynomialFunctionLagrangeForm pflf = new PolynomialFunctionLagrangeForm(x, y);
                
      			print("---\n");        			 
            	for (int row=0; row<numDays; row++){
            		double prev = row==0 ? y[y.length-1] :  forecast[col][row-1];
            		forecast[col][row] = pflf.value(row + daysHistoryUsed);
            		if (cumulative)  forecast[col][row] += prev;
       			 	print("%f\n", forecast[col][row]);        			 
            	}                
        	}
        	
        	return forecast;
		}
	}		
	
	/**
	 * Compute the RMSE for a set of predictions
	 * 
	 * @param prediction
	 * @param actual
	 * @return
	 */
	private static double testPrediction(double[][] prediction, double[][]actual){
		double RMSE = 0f;
		
		if (prediction.length!=actual.length || prediction[0].length != actual[0].length){
			throw new RuntimeException("testPrediction: prediction and actual aren't the same size.");
		}

		double cols = prediction.length;
		double rows = prediction[0].length;
    	for (int col=0; col<cols; col++){
        	for (int row=0; row<rows; row++){
        		RMSE = Math.pow(prediction[col][row] - actual[col][row], 2);
        	}
    	}
		
		return Math.sqrt(RMSE / (rows * cols) );
	}
	
	private static void dumpCsv(double[][] data) throws ParseException{
		int cols = data.length;
		int rows = data[0].length;

		SimpleDateFormat fmt = new SimpleDateFormat("m/d/y");
		Date rowDate = fmt.parse("1/1/13");

    	for (int row=0; row<rows; row++){
    		print(fmt.format(rowDate));

        	for (int col=0; col<cols; col++){
        		print(",");
        		print("%f", data[col][row]);
        	}
    		print("\n");
    		rowDate = new Date(rowDate.getTime() + TimeUnit.DAYS.toMillis(1));
    	}		
	}
	
	private static void print(String format, Object...args){
		System.out.format(format, args);
	}
	
	public static void main(String args[]) throws IOException, ParseException{
		
		double x1[] = {1, 2, 3, 4, 5};
		double y1[] = {2, 5, 4, 5, 8};
		PolynomialFunctionLagrangeForm pf = new PolynomialFunctionLagrangeForm(x1, y1);
		double val = pf.value(9);
		
		if (args.length != 1){
			System.err.println("java HotMess input.csv");
			return;
		}

		String input = args[0];
		CSVReader reader = new CSVReader(new FileReader(input),',');
        String [] nextRow;
		
        ArrayList<ArrayList<Float>> data = new ArrayList<ArrayList<Float>>();
        int rows = 0;
        int cols = 0;

        String header[] = reader.readNext();
        cols = header.length - 1;

        //
        // Lame, yes, but lets just load the data to get the size...
        //
        while ((nextRow = reader.readNext()) != null){
        	rows++;
        }
        reader.close();

        Matrix matrix = new Matrix(rows, cols);
        
        //
        // Now really load the data
        //
        reader = new CSVReader(new FileReader("/Users/mcooper/projects/darkstone/data/raw.csv"),',');

        // Skip the header
        reader.readNext();
        
        //
        int row = 0;
        while ((nextRow = reader.readNext()) != null)
        {
            for(int col=1; col < nextRow.length; col++)
            {
            	String value = nextRow[col];
            	if (value.equals("")){
            		matrix.data[col-1][row] = 0f; // noops
            	}else{
	            	matrix.data[col-1][row] = Double.parseDouble(nextRow[col]);
            	}
            }
            row ++;
        }

        //
        // Compute and output the stddev for each column - a good way of finding related columns...probably not.
        //
        List<Double> stdList = new ArrayList<Double>();
        StandardDeviation std = new StandardDeviation();
        TreeMap<Double, Integer> stdToCol = new TreeMap<Double, Integer>();
        
        for (int col=0; col<cols; col++){
        	double stdDev = std.evaluate(matrix.data[col]);
        	stdToCol.put(stdDev, col);
        	print("%d,%f\n", col, stdDev );
        	stdList.add( stdDev );
        }
        
        System.out.println(stdToCol);
        
        //
        // Get the RMSE of a random prediction
        //
        double[][] training = matrix.getTrainingData(30);
        double[][] holdback = matrix.getTestData(30);
        
        //
        // Predictor #1 - a random number. 
        //
        PredictorRandom randPredictor = new PredictorRandom(training);
        double[][] randPrediction = randPredictor.next30Days();
        print("Random predictor RMSE: %f\n", testPrediction(randPrediction, holdback));

        //
        // Predictor #2 - try averaging variable days of history and
        // 				  see how effective that is at predicting when compared
        //   			  to a hold back set.
        //
        print("Days of History, RMSE\n");
        double minRMSE = Long.MAX_VALUE;
        int optimalDaysHistory = 0;
        for (int daysHistory=9; daysHistory<60; daysHistory++){
	        double[][] holdbackVariable = matrix.getTestData(30);
	        PredictorRollingAverage trendPred = new PredictorRollingAverage(training, daysHistory);
	        double[][] trendPredictions = trendPred.nextXDays(30);
	        double rmse = testPrediction(trendPredictions, holdbackVariable);
	        if (minRMSE > rmse){
	        	optimalDaysHistory = daysHistory;
	        	minRMSE = rmse;
	        }
	        print("%d, %f\n", daysHistory, testPrediction(trendPredictions, holdbackVariable) );
        }

        print("Optional days: %d / min RMSE: %f\n", optimalDaysHistory, minRMSE);
        
        //
        // Dump out actual next 30 day predictions using the 
        // number of days of history used we previously determined to be the
        // best number of days to use...     
        //
        print("Next 30 day predictions...\n");
        PredictorRollingAverage trendP = new PredictorRollingAverage(matrix.data, optimalDaysHistory);
        double[][] next30Days = trendP.nextXDays(30);
        dumpCsv(next30Days);
        
        //
        // Try and fit the curve...
        //
        if (false){
	        print("Days of History, RMSE\n");
	        for (int daysHistory=Math.min(30, 2520); daysHistory<2521; daysHistory++){
		        double[][] holdbackVariable = matrix.getTestData(daysHistory);
		        PredictorPFLF trendPred = new PredictorPFLF(training, daysHistory);
		        double[][] trendPredictions = trendPred.nextXDays(30);
		        print("%d, %f\n", daysHistory, testPrediction(trendPredictions, holdbackVariable) );
	        }
        }        
	}
}
